'use strict';

const express = require('express');

const app = express();

app.set('views', require('path').join(__dirname, 'views'));
app.set('view engine', 'pug');

// pdfs
app.use('/generate-pdf', require('./pdf-generator/crud'));

//app.use('/pdfs', require('./generate-pdf/api'));

// Redirect root to /books
// app.get('/', (req, res) => {
//   res.redirect('/pdfs');
// });

app.get('/errors', () => {
  throw new Error('Test exception');
});

app.get('/logs', (req, res) => {
  console.log('Hey, you triggered a custom log entry. Good job!');
  res.sendStatus(200);
});

// Start the server
const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});

module.exports = app;
