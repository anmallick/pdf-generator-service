# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This service returns filled PDFs with the information sent. Current forms supported - Travel Form, Requisition Form.
* To start, clone this project.

### How do I setup? ###

* Make sure Nodejs is installed. If not, get it [here](https://nodejs.org/en/download/).
* Make sure docker is installed. If not, get it [here](https://docs.docker.com/get-docker/).
* Make sure you have an account in docker. If not, create [here](https://hub.docker.com/signup).
* Download Postman App to debug from [here](https://www.postman.com/downloads/).

### How do I run? ###

* Once you clone, go to the directory and run ```npm install```.
* You can run locally using ```npm start```.
* Test the API by making a POST request to ```http://localhost:8080/generate-pdf?form=<Form>``` using [this](https://docs.google.com/document/d/1aSIYZMy7MTx9zEKiTMui37I_JeVm4zVDPH0uVT7mHyo/edit?usp=sharing) API specification.
* Once tested, build the image using
```
docker build -t <hub-user>/<repo-name>:<tag> .
```
* Run the image locally using
```
docker run -d -p 9000:8080 <hub-user>/<repo-name>:<tag>
```

* Push the image using
```
 docker push <hub-user>/<repo-name>:<tag>
```

### How do I deploy? ###

* SSH to the machine ```130.245.171.228```.
* Pull the image using ```docker pull ani3993/pdf-generator:1.0.0```.
* Then run the image in a container using
```
docker run -d -p 9000:8080 <hub-user>/<repo-name>:<tag>
```

### Troubleshooting ###

* To check logs, run ```docker logs <ContainerID>```.
