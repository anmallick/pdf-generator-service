const objectMapper = require('object-mapper');

module.exports = {
	//mapper functions for different forms
    requisition:function(body, mapper) {
    	var mapped_fields = objectMapper(body, mapper.general_fields)
    	if (body.item_information) {
    		for(var i = 0; i < body.item_information.length; i++){
    			var keys = Object.keys(body.item_information[i])
    			for (key in keys){
    				mapped_fields[mapper.item_information[i][keys[key]]] = body.item_information[i][keys[key]]
    			}
    		}
    	}
        return mapped_fields
    },
    travel:function(body, mapper) {
    	var mapped_fields = objectMapper(body, mapper)
    	return mapped_fields
    }
}