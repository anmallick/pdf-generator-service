const express = require('express');
const bodyParser = require('body-parser');
const pdfFiller   = require('pdffiller-stream');
const yaml = require('js-yaml');
const fs = require('fs');
const helper = require('./helper.js')
const mapper = require('./mapper.js')
const uuid = require('uuid')
require('log-timestamp');
//const db = require('./firestore');

const router = express.Router();
properties = "";
try {
  let fileContents = fs.readFileSync('./form-properties.yaml', 'utf8');
  properties = yaml.safeLoad(fileContents)
} catch (e) {
  console.log(e)
}

// Automatically parse request body as JSON
router.use(bodyParser.json());

/**
 * POST /generate-pdf/
 *
 * Create a new PDF using the provided information.
 */
router.post('/', async (req, res) => {
  var formName = helper.cleanText(req.query.form);

  if (formName == "") {
    console.log('Received invalid request without param: form.');
    return res.status(400).json({
      status: 400,
      error: "parameter 'form' is missing."
    });
  }
  console.log('Received request for form: '+formName);
  try {
    if (properties[formName]){
      var formMapper = JSON.parse(fs.readFileSync(properties[formName].mapper, 'utf8'))
      var pdfFields = mapper[formName](req.body, formMapper)
      var sourcePDF = properties[formName].pdfPath
      var filename = uuid.v1() + '.pdf';
      // nameRegex = null;
      // const FDF_data = pdfFiller.generateFDFTemplate(sourcePDF, nameRegex).then((fdfData) => {
      //   console.log(fdfData);
      // }).catch((err) => {
      //   console.log(err);
      // });
      // pdfFiller.fillForm(sourcePDF, pdfFields).then((stream) => {
      //     // use the outputStream here;
      //     // will be instance of stream.Readable
      //     res.setHeader('Content-disposition', 'attachment; filename="' + filename + '"');
      //     res.setHeader('Content-type', 'application/pdf');

      //     stream.pipe(res)
      // }).catch((err) => {
      //     console.log(err);
      // });
      const shouldFlatten = false;
      pdfFiller.fillFormWithFlatten(sourcePDF, pdfFields, shouldFlatten)
      .then((outputStream) => {
          // etc, same as above
          console.log('Processed PDF successfully.');
          res.setHeader('Content-disposition', 'attachment; filename="' + filename + '"');
          res.setHeader('Content-type', 'application/pdf');
          outputStream.pipe(res);
        });
      } else {
        console.log('Form is invalid.');
        res.status(404).json({
          status: 404,
          error: "form not found."
        });
      }
    } catch (e) {
      console.log('Error encountered.');
      console.log(e);
      res.status(500).json({
        status: 500,
        error: "Something went wrong."
      });
    }
});


module.exports = router;
